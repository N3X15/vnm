import re
import os
from typing import Optional, List
from pip._internal.models.direct_url import VcsInfo

from pip._vendor.packaging.specifiers import Specifier, SpecifierSet
from vnm.repos import *
from vnm.constraint import Constraint, EEqualityType

# Because I have finally grown tired of trying to parse this shit.
from pip._vendor.packaging.requirements import Requirement as PipRequirement
from pip._internal.models.link import Link as PipLink
from pip._internal.utils.direct_url_helpers import direct_url_from_link as pip_direct_url_from_link

class Package(object):
    REGEX_CONSTRAINTS=re.compile(r'(?P<equality>(==|>=|<=|>|<))(?P<version>[0-9\.]+)')
    REGEX_PACKAGENAME=re.compile(r'^([a-zA-Z0-9_\-\.]+)')

    @classmethod
    def FromJSON(cls, data: dict) -> 'Package':
        p = cls()
        p.deserialize(data)
        return p

    def __init__(self, pkgID: Optional[str] = None):
        self.id: str = ''
        self.development_mode: bool = False # -e
        self.repo: Repo = None
        self.priority: int = 50
        self.constraints: List[Constraint] = []
        self.freezeVersion: Optional[Constraint] = None
        if pkgID is not None:
            if pkgID.startswith('-e '):
                self.development_mode = True
                pkgID=pkgID[3:]
            
            req: Optional[PipRequirement] = None
            lnk: Optional[PipLink] = None
            try:
                req = PipRequirement(pkgID)
            except Exception as e1:
                try:
                    lnk = PipLink(pkgID)
                    req = PipRequirement(lnk.egg_fragment)
                    req.url = pkgID
                except Exception as e2:
                    print(f'Failed to parse requirement: {pkgID!r}')
                    print(repr({
                        'extras': req.extras,
                        'marker': req.marker,
                        'name': req.name,
                        'specifier': req.specifier,
                        'url': req.url,
                    }))
                    raise e1
            is_file: bool = False
            is_vcs: bool = False
            if req and req.url and not lnk:
                lnk = PipLink(req.url)
            if lnk:
                is_file = lnk.is_file
                is_vcs = lnk.is_vcs

            if is_file:
                self.repo = WheelRepo(pkgID)
                self.id = self.repo.getID()
            elif is_vcs:
                url = pip_direct_url_from_link(lnk, '.')
                if url.info.vcs == 'git':
                    #if pkgID.startswith('git+') or ' @ git+' in pkgID:
                    self.repo = GitRepo()
                    self.repo.from_direct_url(url)
                    if self.id is None:
                        self.id = self.repo.pkgid
                else:
                    raise Exception(f'ERROR: Unknown/unimplemented pip VCS type {url.info.vcs!r}')
            else:
                '''
                repo = None
                if ' @ ' in pkgID:
                    pkgID, repo = pkgID.split('@', 1)
                    pkgID = pkgID.strip()
                    repo = repo.strip()
                m = self.REGEX_PACKAGENAME.match(pkgID)
                assert m is not None, f'Invalid package name {pkgID!r}'
                self.id = m[1]
                if repo is not None:
                    assert repo.startswith('git+')
                    self.repo = GitRepo(repo, knownPackageID=self.id)

                for cm in self.REGEX_CONSTRAINTS.findall(pkgID):
                    c = Constraint()
                    c.setEquality(cm[1])
                    c.setVersion(cm[2])
                    self.constraints += [c]
                '''
                # build==0.6.0.post1
                # {'extras': set(), 'marker': None, 'name': 'build', 'specifier': <SpecifierSet('==0.6.0.post1')>, 'url': None}
                # pybuildtools @ git+https://gitlab.com/N3X15/python-build-tools.git@f8b6346461eb150ca9a0e053b21d715e885a7281
                # {'extras': set(), 'marker': None, 'name': 'pybuildtools', 'specifier': <SpecifierSet('')>, 'url': 'git+https://gitlab.com/N3X15/python-build-tools.git@f8b6346461eb150ca9a0e053b21d715e885a7281'}
                self.id = req.name
                self.constraints = []
                s: Specifier
                for s in req.specifier:
                    c = Constraint()
                    c.setEquality(s.operator)
                    c.setVersion(s.version)
                    self.constraints.append(c)

    def __str__(self) -> str:
        o = f'{self.id}'
        o += f' ({self.repo})' if self.repo is not None else ''
        if self.development_mode:
            o += ' [developer mode (-e)]'
        return o

    def serialize(self) -> dict:
        o = {}
        if self.development_mode:
            o['development'] = True
        if self.repo is not None:
            o['repo'] = self.repo.serialize()
        if len(self.constraints) > 0:
            o['constraints'] = []
            for constraint in self.constraints:
                o['constraints'] += [constraint.serialize()]
        if self.freezeVersion is not None:
            o['frozen-at'] = self.freezeVersion.version
        return o

    def freezeAt(self, constraint: Constraint) -> None:
        self.freezeVersion = constraint
        if self.repo is not None:
            self.repo.freeze(constraint)

    def thaw(self) -> None:
        self.freezeVersion = None
        if self.repo is not None:
            self.repo.thaw()

    def deserialize(self, data: dict) -> None:
        self.development_mode = data.get('development', False)
        self.repo = None
        if 'repo' in data:
            #print(repr(data['repo']))
            rt = data['repo']['type']
            if rt == 'git':
                self.repo = GitRepo()
            elif rt == 'wheel':
                self.repo = WheelRepo()
            else:
                print(f"Error: Unknown repo.type={rt!r}")
            if self.repo is not None:
                self.repo.deserialize(data['repo'])
                #print(repr(self.repo), str(self.repo))
        if 'constraints' in data:
            for cdata in data['constraints']:
                c = Constraint()
                c.deserialize(cdata)
                self.constraints += [c]
        if 'frozen-at' in data:
            c = Constraint()
            if isinstance(data['frozen-at'], str):
                c.setEquality(EEqualityType.EQUAL)
                c.setVersion(data['frozen-at'])
            else:
                c.deserialize(data['frozen-at'])
            self.freezeAt(c)

    def constraintsToStr(self) -> str:
        o: str = ''
        if self.repo is None or self.repo.shouldDisplayConstraints():
            if self.freezeVersion is not None:
                o += self.freezeVersion.toRequirement()
            else:
                o += ','.join([x.toRequirement() for x in self.constraints])
        return o

    def toRequirement(self) -> str:
        o = ''
        if self.development_mode:
            o += '-e '
        if self.repo is not None:
            o += self.repo.toRequirement(self)
        else:
            o += self.id
        if self.repo is None or self.repo.shouldDisplayConstraints():
            if self.freezeVersion is not None:
                o += self.freezeVersion.toRequirement()
            else:
                o += ','.join([x.toRequirement() for x in self.constraints])
        return o
    
    def toPipRequirement(self) -> str:
        req = PipRequirement(self.id)
        if self.repo is not None:
            req.url = self.repo.getPipLinkURL()
        if self.repo is None or self.repo.shouldDisplayConstraints():
            specs = []
            if self.freezeVersion is not None:
                specs.append(self.freezeVersion.asPipSpecifier())
            else:
                specs = [x.asPipSpecifier() for x in self.constraints]
            req.specifier = SpecifierSet()
            req.specifier._specs = frozenset(specs)
        return req

    def toPipArgs(self) -> List[str]:
        o = []
        if self.development_mode:
            o += ['-e']
        if self.repo is not None:
            o += self.repo.toPipArgs(self)
        else:

            o += [self.id]
        if self.repo is None or self.repo.shouldDisplayConstraints():
            if self.freezeVersion is not None:
                o[-1] += self.freezeVersion.toRequirement()
            else:
                o[-1] += ','.join([x.toRequirement() for x in self.constraints])
        return o

    def fromDirectURL(self, data: dict) -> None:
        '''Loads PEP 610 data from .dist-info/direct_url.json'''
        return