from typing import List
class Repo(object):
    TYPEID = ''
    def __init__(self) -> None:
        pass

    def serialize(self) -> dict:
        return {'type': self.TYPEID}

    def deserialize(self, data: dict) -> None:
        return

    def toRequirement(self, parent: 'Package') -> str:
        return ''

    def toPipArgs(self, parent: 'Package') -> List[str]:
        return []

    def shouldDisplayConstraints(self) -> bool:
        return True

    def freeze(self, at: 'Constraint') -> None:
        return

    def thaw(self) -> None:
        return

    def fromDirectURL(self, data: dict) -> None:
        '''Loads PEP 610 data from .dist-info/direct_url.json'''
        return

_ALL = []
_ALL_BY_TYPE = {}
def register_repo_type(typ) -> None:
    _ALL.append(typ)
    _ALL_BY_TYPE[typ.TYPEID] = typ