from typing import Type
from .repo import Repo, _ALL_BY_TYPE
from .wheel import WheelRepo
from .git import EGitRefType, GitRepo

def getRepoTypeByPEP610ID(vcs: str) -> Type[Repo]:
    return _ALL_BY_TYPE[vcs]