# 0.5.2
* Now parses constraints using `pip._vendors.packaging` and friends
* `vnm` checks venv for needed updates

## Added
* Added `--git` and `--lfs` to `vnm init`
* `pip`, `wheel`, and `setuptools` are checked before install
* `pip`, `wheel`, and `setuptools` are included in the freeze process

## Fixed
* `vnm add` no longer triggers a `get_filename()` crash if a package already exists.
* Windows fixes.

# 0.3.2
* Simplified `frozen-at` syntax.
* Fixed crash during initial install.

# 0.3.1
* Released September 3rd, 2020.

## Additions
* Added `frozen-at` to vnm.yml (prevents collisions with constraints).
* Added `--constraints` to `vnm thaw` command to permit nuking of constraints.

## Fixes
* `vnm remove` no longer reinstalls uninstalled packages.

# 0.3.0

## Additions

* `freeze` - Freezes package versions in the YML file.
* `thaw` - Unfreezes package versions.
